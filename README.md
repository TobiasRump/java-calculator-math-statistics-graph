## Studiumprojekt: Taschenrechner

Das Projekt wurde i.Z.m. Herrn [Hung Dang-Qouc](https://gitlab.com/hungdangquoc)  entwickelt. \
Der Taschenrechner wurde innerhalb des Modul _"Module Fortgeschrittene Programmierkonzepte"_ prgorammiert.
### Der Taschenrechner enthält folgende Funktionalitäten:
- Mathematische Berechnung
- Statistik
- Monter Carlo Simulation von der Zahl PI
- Graphische Funktionen

### Beispielbilder aus der Software
![displays](https://gitlab.com/TobiasRump/java-calculator-math-statistics-graph/-/raw/master/Screencast/pics/MathF.PNG)
![displays](https://gitlab.com/TobiasRump/java-calculator-math-statistics-graph/-/raw/master/Screencast/pics/GraphF.PNG)
![displays](https://gitlab.com/TobiasRump/java-calculator-math-statistics-graph/-/raw/master/Screencast/pics/MontF.PNG)
![displays](https://gitlab.com/TobiasRump/java-calculator-math-statistics-graph/-/raw/master/Screencast/pics/StatF.PNG)
![displays](https://gitlab.com/TobiasRump/java-calculator-math-statistics-graph/-/raw/master/Screencast/pics/StatFResult.PNG)

