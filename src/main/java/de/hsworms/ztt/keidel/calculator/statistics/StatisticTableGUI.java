package de.hsworms.ztt.keidel.calculator.statistics;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;

//TODO: Tabelle lässt sich nicht scrollen

public class StatisticTableGUI extends JPanel {

    private final Color background_color = new Color(162, 175, 119);
    private final String[] column_name = new String[]{"ID", "x", "Frequency"};
    private String[][] content = {{"1", "", "1"}, {"2", "", "1"}, {"3", "", "1"}};
    private JTable table;
    private final JPanel table_panel = new JPanel();
    private final JTextField math_expression = new JTextField("15");

    public StatisticTableGUI() {

        JScrollPane scrollpane;
        JButton delete_all_button;
        JButton delete_button;
        JButton add_button;

        //SetUp
        setPropertiesForGUI();
        setPropertiesForMathExpression();
        updateTable();
        scrollpane = setPropertiesForScrollPane(table);
        setPorpertiesForTablePanel();
        add_button = setPropertiesForButtons("+"); //Add row button
        delete_button = setPropertiesForButtons("-");//Delete one row button
        delete_all_button = setPropertiesForButtons("DEL"); //Delete all rows button

        //Screen-Elements hierarchy
        add(table_panel);
        table_panel.add(scrollpane);
        add(add_button);
        add(delete_button);
        add(delete_all_button);
    }

    private JButton setPropertiesForButtons(String button_name) {
        JButton new_button = new JButton(button_name);

        new_button.setPreferredSize(new Dimension(70, 25));
        new_button.setBackground(background_color);

        switch (button_name) {
            case "+":
                new_button.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        int row_index = content.length;
                        String[][] new_content = new String[row_index + 1][3];

                        resetEditSelection();

                        for (int index = 0; index < row_index; index++) {
                            System.arraycopy(content[index], 0, new_content[index], 0, 3);
                        }

                        //new data row
                        new_content[row_index][0] = String.valueOf(row_index + 1);
                        new_content[row_index][1] = "";
                        new_content[row_index][2] = "1";
                        content = new_content;
                        replaceTablePanel();
                    }
                });
                break;

            case "-":
                new_button.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        int row_index = content.length;

                        //Cant be lower than 1 row
                        if (row_index <= 1) return;

                        String[][] new_content = new String[row_index - 1][3];

                        for (int i = 0; i < row_index - 1; i++) {
                            System.arraycopy(content[i], 0, new_content[i], 0, 3);
                        }

                        content = new_content;
                        replaceTablePanel();
                    }
                });
                break;

            case "DEL":
                new_button.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        content = new String[][]{{"1", "", "1"}};
                        replaceTablePanel();
                    }
                });
        }
        return new_button;
    }

    private void replaceTablePanel() {
        table_panel.removeAll();           //Delete old table
        updateTable();                     //Update Table
        table_panel.add(CreateNewTable()); //Add the now created table
        table_panel.revalidate();           //update component in the panel
    }

    private void setPorpertiesForTablePanel() {
        table_panel.setPreferredSize(new Dimension(320, 75));
        table_panel.setBackground(background_color);

    }

    private JScrollPane setPropertiesForScrollPane(JTable table_in) {
        JScrollPane scrollPane = new JScrollPane(table_in);
        scrollPane.setAutoscrolls(true);
        scrollPane.setBackground(new Color(162, 175, 119));
        scrollPane.setPreferredSize(new Dimension(320, 70));
        return scrollPane;
    }

    private void setPropertiesForMathExpression() {
        math_expression.setPreferredSize(new Dimension(160, 25));
        math_expression.setBackground(background_color);
        math_expression.setBorder(BorderFactory.createLineBorder(Color.black));
    }

    private void setPropertiesForGUI() {
        Dimension minDisplaySize = new Dimension(335, 140);
        setSize(minDisplaySize);
        setBackground(background_color);
        setPreferredSize(new Dimension(335, 120));
        setLayout(new FlowLayout(FlowLayout.LEFT));
        setBackground(background_color);
    }

    private void updateTable() {
        table = new JTable(content, column_name);
        table.getTableHeader().setReorderingAllowed(false);
        setTableConfigs(table);
        setColumnContentCenter(table);
        setTableColumnWidths(table);
    }

    private JScrollPane CreateNewTable() {
        JScrollPane new_pane = new JScrollPane(table);

        new_pane.setAutoscrolls(true);
        new_pane.getVerticalScrollBar().addAdjustmentListener(new AdjustmentListener() {
            public void adjustmentValueChanged(AdjustmentEvent e) {
                e.getAdjustable().setValue(e.getAdjustable().getMaximum());
            }
        });

        new_pane.setBackground(new Color(162, 175, 119));
        new_pane.setPreferredSize(new Dimension(320, 70));

        return new_pane;
    }

    private void setTableConfigs(JTable table) {
        table.setColumnSelectionAllowed(false);
        table.getTableHeader().setBackground(new Color(162, 175, 119));
        table.setBackground(background_color);
    }

    private void setColumnContentCenter(JTable table) {
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);
        table.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);
        table.getColumnModel().getColumn(1).setCellRenderer(centerRenderer);
        table.getColumnModel().getColumn(2).setCellRenderer(centerRenderer);
    }

    private void setTableColumnWidths(JTable table) {
        table.getColumnModel().getColumn(0).setPreferredWidth(1);
        table.getColumnModel().getColumn(1).setPreferredWidth(130);
        table.getColumnModel().getColumn(2).setPreferredWidth(130);
    }

    private void resetEditSelection() {
        table.editCellAt(0, 0);
        table.removeEditor();
    }

    public JTable getStatisticTable() {
        return table;
    }
}
