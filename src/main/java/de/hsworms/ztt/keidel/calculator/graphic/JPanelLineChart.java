package de.hsworms.ztt.keidel.calculator.graphic;

import de.hsworms.ztt.keidel.calculator.ReversePolishNotation;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class JPanelLineChart extends JPanel {


    private static final int SIZE = 100;
    private static final Double FROM = -5.;
    private static final Double TO = 5.;
    private static final Double STEPS = .1;
    private static final Color PRIMARYCOLOR = new Color(162, 175, 119);
    private List<Double> listValues;

    public JPanelLineChart() {
    }

    public JPanelLineChart(List<Double> scores) {
        this.listValues = scores;
        setBackground(PRIMARYCOLOR);
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D copyGraphic = (Graphics2D) g;
        copyGraphic.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        copyGraphic.setColor(Color.BLACK);

        //draw x and y axis
        copyGraphic.drawLine(0, SIZE / 2, SIZE, SIZE / 2);
        copyGraphic.drawLine(SIZE / 2, 0, SIZE / 2, SIZE);

        Double axisDistance = SIZE / ((Math.abs(FROM) + Math.abs(TO)));

        //draw x and y axis-devision
        for (Double counterDistance = 0.; counterDistance < SIZE; counterDistance += axisDistance) {
            copyGraphic.drawLine(counterDistance.intValue(), (SIZE / 2) - 2, counterDistance.intValue(), (SIZE / 2) + 2);
            copyGraphic.drawLine((SIZE / 2) - 2, counterDistance.intValue(), (SIZE / 2) + 2, counterDistance.intValue());
        }

        //distance is needed to get the pixel-corrdinate
        Double distance = SIZE / ((Math.abs(FROM) + Math.abs(TO)) / STEPS);

        //set values for graph
        List<Point> graphPoints = new ArrayList<Point>();
        for (int currentValue = 0; currentValue < listValues.size(); currentValue++) {

            //Filters out the values that cannot be seen in the coordinate system
            if (listValues.get(currentValue) == null) {
                continue;
            }

            //set the corrdinate for the Graph into point-instance
            int x1 = currentValue * distance.intValue();
            int y1 = SIZE - ConvertToPixel(listValues.get(currentValue));

            graphPoints.add(new Point(x1, y1));
        }

        //set fix color for the graph
        copyGraphic.setColor(Color.BLUE);
        copyGraphic.setStroke(new BasicStroke(1f));

        //Draw graph
        for (int i = 0; i < graphPoints.size() - 1; i++) {
            int x1 = graphPoints.get(i).x;
            int y1 = graphPoints.get(i).y;
            int x2 = graphPoints.get(i + 1).x;
            int y2 = graphPoints.get(i + 1).y;
            copyGraphic.drawLine(x1, y1 - SIZE / 2, x2, y2 - SIZE / 2);
        }
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(SIZE, SIZE);
    }

    //calculate the f(x)-Value from Function(infix) and add to a List
    private List<Double> CreateListOfValueFromInfix(String infix)
    {
        List<Double> listValues = new ArrayList<Double>();
        double y = 0;

        if(infix == "")
            return listValues;

        //operate values for graphic
        for (double x = FROM; x <= TO; x += STEPS) {
            try {
                y = ReversePolishNotation.getResult(infix.replace("x", String.valueOf(x)));
            } catch (Exception ex) {
            }

            if (y < FROM || y > TO) {
                listValues.add(null);
                continue;
            }
            listValues.add(y);
        }

        return listValues;
    }

    //Create the LineChart and the desciption of the infix
    public JPanel createLineChart(String infix) throws IOException, FontFormatException {
        List<Double> listValues = CreateListOfValueFromInfix(infix);

        //create MainPanel for linechart and description
        JPanel mainPanel = new JPanel();
        mainPanel.setBackground(new Color(162, 175, 119));
        mainPanel.setPreferredSize(new Dimension(335, 120));

        //create lineChart
        JPanelLineChart lineChartPanel = new JPanelLineChart(listValues);
        lineChartPanel.setPreferredSize(new Dimension(120, 120));

        //create scroller for description, if the screen is too big
        rightPanel = new RightDisplayPanel(infix);
        JScrollPane scroll = new JScrollPane(rightPanel);
        scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
        scroll.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        scroll.setPreferredSize(new Dimension(215, 120));
        scroll.setBorder(new EmptyBorder(0, 0, 0, 0));

        //add panels
        mainPanel.add(lineChartPanel);
        mainPanel.add(scroll);

        return mainPanel;
    }


    private RightDisplayPanel rightPanel;
    public JLabel getOutputLabel()
    {
        return rightPanel.GetOutputLabel();
    }


    private int ConvertToPixel(Double value) {
        Double onePixelAsValue = SIZE / ((Math.abs(FROM) + Math.abs(TO)));
        Double result = onePixelAsValue * value;
        return result.intValue();
    }
}

//Is needed to show the infix inside a Panel
class RightDisplayPanel extends JPanel {

    private JLabel label;

    public RightDisplayPanel(String infix) throws IOException, FontFormatException {

        setBackground(new Color(162, 175, 119));
        setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
        JLabel title = new JLabel("F(x) = ");


        label = new JLabel(infix);
        String path = System.getProperty("user.dir") + "\\src\\main\\java\\de\\hsworms\\ztt\\keidel\\calculator\\gui\\DS-DIGI.TTF";
        Font customFont = Font.createFont(Font.TRUETYPE_FONT, new File(path)).deriveFont(30f);
        label.setFont(customFont);
        title.setFont(customFont);

        add(title);
        add(label);
    }

    public JLabel GetOutputLabel()
    {
        return label;
    }
}