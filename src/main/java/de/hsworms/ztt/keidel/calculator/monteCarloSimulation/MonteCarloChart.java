package de.hsworms.ztt.keidel.calculator.monteCarloSimulation;


import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MonteCarloChart extends JPanel implements Runnable {

    private final int SIZE = 95;
    private final int BORDER_GAP = SIZE / 10;
    private final int SIZE_DIVIDEND = 1; // 1 = FULLSCREEN
    private static final Color PRIMARYCOLOR = new Color(162, 175, 119);
    private double pi;
    private boolean isRunning = true;
    private Thread animator;

    // Number of drops
    private int n = 2500;
    // Number of drops inside the quartercircle
    private int v = 0;
    // corrdinate for the point-object
    private double x, y;

    private JLabel printLabel = new JLabel();

    public MonteCarloChart() {
        printLabel.setText("<html><strong>Monte Carlo Simulation</strong><br><br>" +
                "\u2248\u03C0 = 4 * v / n<br>" +
                "v = (Punkte im Viertelkreis)<br>" +
                "n = (Gesamtdurchlauf)</html>");

       printLabel.setFont(new Font("Serif", Font.PLAIN, 12));
        setBackground(PRIMARYCOLOR);
    }


    @Override
    public void addNotify() {
        super.addNotify();
        animator = new Thread(this);
        animator.start();
    }

    boolean isTemp = true;
    boolean isTemp2 = false;
    int counter = 0;

    @Override
    public void paintComponent(Graphics g) {
        counter++;

        //termination condition for the paint-routine
        if(counter >= n)
        {
            printLabel.setText("<html><strong>Monte Carlo Simulation</strong><br><br>" +
                    "\u2248\u03C0 = 4 * v / n<br>" +
                     "<strong>" + pi + "</strong> = 4 * " + v + " / " + n + "<br>"+
                    "v = (Punkte im Viertelkreis)<br>" +
                    "n = (Gesamtdurchlauf)</html>");

            return;
        }

        //call recursive the paintcomponent-method
        updateUI();
        Graphics2D copyGraphic = (Graphics2D) g;
        copyGraphic.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        copyGraphic.setColor(Color.BLACK);

        int size = SIZE / SIZE_DIVIDEND;

        //just call one time
        if (isTemp2 && isTemp) {
            //draw background
            g.setColor(PRIMARYCOLOR);
            g.fillRect(0, 0, SIZE + 2 * BORDER_GAP, SIZE + 2 * BORDER_GAP);

            //draw circle
            copyGraphic.setColor(Color.black);
            copyGraphic.drawOval((-size + BORDER_GAP), size + (-size + BORDER_GAP), 2 * size, 2 * size);

            //draw x and y axis
            copyGraphic.setColor(Color.black);
            copyGraphic.drawLine(BORDER_GAP - SIZE / 10, SIZE + BORDER_GAP, SIZE + BORDER_GAP + SIZE / 10, SIZE + BORDER_GAP);
            copyGraphic.drawLine(BORDER_GAP, SIZE + BORDER_GAP + SIZE / 10, BORDER_GAP, BORDER_GAP - SIZE / 10);

            isTemp = false;
        }

        isTemp2 = true;

        //set random (0 and 1) to x and y
        x = Math.random();
        y = Math.random();

        //Function returns the square root of the sum of the arguments squared.
        if (Math.hypot(x, y) <= 1) {
            v = v + 1;
            copyGraphic.setColor(Color.RED);
        } else {
            copyGraphic.setColor(Color.BLUE);
        }


        int xScale = (int) (x * size) + BORDER_GAP;
        int yScale = size - (int) (y * size) + BORDER_GAP;

        //set the points into the graphic
        copyGraphic.drawOval(xScale, yScale, 1, 1);
        copyGraphic.fillOval(xScale, yScale, 1, 1);

        //calculates the approximation of Pi
        pi = 4 * (double) v / n;
        String msg = n + " Tropfen, davon " + v + " Tropfen im Viertelkreis, Pi etwa " + pi;
        System.out.println(msg);
    }

    @Override
    public void run() {}


    public JPanel getMonteCarloPanel() {
        //create MainPanel for linechart and description
        JPanel mainPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        mainPanel.setBackground(new Color(162, 175, 119));
        mainPanel.setPreferredSize(new Dimension(335, 120));

        //create lineChart
        this.setPreferredSize(new Dimension(120, 120));

        //add panels
        mainPanel.add(this);

        JPanel printPanel = new JPanel(new GridBagLayout());
        printPanel.add(printLabel);
        printPanel.setBackground(new Color(162, 175, 119));
        printPanel.setPreferredSize(new Dimension(200, 120));
        printPanel.setBorder(new EmptyBorder(0, 0, 0, 0));
        mainPanel.add(printPanel);

        return mainPanel;
    }
}
