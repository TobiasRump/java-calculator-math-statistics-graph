package de.hsworms.ztt.keidel.calculator.logarithmus;

import de.hsworms.ztt.keidel.calculator.ReversePolishNotation;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FilterLogarithm
{
    /**
     * checks the infix according to logarithm
     * @param infix
     * @return
     */
    public static Boolean IsLogarithm(String infix)
    {
        String pattern = "log";
        Pattern r = Pattern.compile(pattern);
        Matcher m = r.matcher(infix);

        if (m.find( )) return true;

        return false;
    }

    /**
     * replace the content inside the logarithm and calculates it
     * @param infix
     * @return
     */
    public static String GetReplacedInfixString(String infix)
    {
        String pattern = "log\\(-?[0-9. +*\\/-]+\\)";
        Pattern r = Pattern.compile(pattern);
        Matcher m = r.matcher(infix);
        if (m.find( ))
        {
            String test = m.group(0);
            String ns = NewString(m.group(0));
            return infix.replace(m.group(0), ns);
        }

        return "";
    }

    /**
     * helper-method to calculate the content inside the logarithm
     * @param infix
     * @return
     */
    private static String NewString(String infix)
    {
        String pattern = "\\(.+\\)";
        Pattern r = Pattern.compile(pattern);
        Matcher  m = r.matcher(infix);

        String temp = "";
        if (m.find( )) { temp = m.group(0);}

        String newString = "";
        try
        {
            newString = String.valueOf(round(Math.log(ReversePolishNotation.getResult(temp)), 2));
        }
        catch (Exception ex)
        {
            ex.fillInStackTrace();
        }

        return newString;
    }


    public static double round(double number, int index) {
        return (double) ((int)number + (Math.round(Math.pow(10,index)*(number-(int)number)))/(Math.pow(10,index)));
    }
}
