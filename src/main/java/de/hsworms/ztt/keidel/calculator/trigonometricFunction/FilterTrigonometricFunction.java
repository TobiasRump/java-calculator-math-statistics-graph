package de.hsworms.ztt.keidel.calculator.trigonometricFunction;

import de.hsworms.ztt.keidel.calculator.ReversePolishNotation;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FilterTrigonometricFunction
{
    public enum Type {
        SIN, COS, TAN
    }

    /**
     * checks the infix according to trigonometric function
     * @param infix
     * @return
     */
    public static Boolean IsTrigonometricFunctionAvailable(String infix)
    {

        String pattern = "sin";
        Pattern r = Pattern.compile(pattern);
        Matcher m = r.matcher(infix);

        if (m.find( )) return true;

        pattern = "cos";
        r = Pattern.compile(pattern);
        m = r.matcher(infix);
        if (m.find( )) return true;

        pattern = "tan";
        r = Pattern.compile(pattern);
        m = r.matcher(infix);
        if (m.find( )) return true;

        return false;
    }


    /**
     * replace the content inside the logarithm and calculates it
     * @param infix
     * @return
     */
    public static String GetReplacedInfixString(String infix)
    {

        String pattern = "sin\\(-?[0-9. +*\\/-]+\\)";
        Pattern r = Pattern.compile(pattern);
        Matcher m = r.matcher(infix);
        if (m.find( ))
        {
            String ns = NewString(Type.SIN, m.group(0));
            return infix.replace(m.group(0), ns);
        }

        pattern = "cos\\(-?[0-9. +*\\/-]+\\)";
        r = Pattern.compile(pattern);
        m = r.matcher(infix);
        if (m.find( ))
        {
            String ns = NewString(Type.COS, m.group(0));
            return infix.replace(m.group(0), ns);
        }

        pattern = "tan\\(-?[0-9. +*\\/-]+\\)";
        r = Pattern.compile(pattern);
        m = r.matcher(infix);
        if (m.find( ))
        {
            String ns = NewString(Type.TAN, m.group(0));
            return infix.replace(m.group(0), ns);
        }

        return "";
    }

    /**
     * helper-method to calculate the content inside the trigonometric function
     * @param infix
     * @return
     */
    private static String NewString(Type operator, String infix)
    {
        String pattern = "\\(.+\\)";
        Pattern r = Pattern.compile(pattern);
        Matcher  m = r.matcher(infix);

        String temp = "";
        if (m.find( )) { temp = m.group(0);}

        String newString = "";
        try
        {
            switch (operator)
            {
                case SIN: newString = String.valueOf(round(Math.sin(Math.toRadians(ReversePolishNotation.getResult(temp))), 2));
                    break;
                case COS:newString = String.valueOf(round(Math.cos(Math.toRadians(ReversePolishNotation.getResult(temp))), 2));
                    break;
                case TAN:
                {
                    double result = ReversePolishNotation.getResult(temp);
                    if(result == 90.)
                    {
                        throw new Exception();
                    }
                    newString = String.valueOf(round(Math.tan(Math.toRadians(result)), 2));
                }
                    break;
            }

        }
        catch (Exception ex)
        {
            ex.fillInStackTrace();
        }

        return newString;
    }


    public static double round(double number, int index) {
        return (double) ((int)number + (Math.round(Math.pow(10,index)*(number-(int)number)))/(Math.pow(10,index)));
    }
}
