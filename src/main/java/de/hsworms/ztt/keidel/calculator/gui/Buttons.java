package de.hsworms.ztt.keidel.calculator.gui;

import javax.swing.*;
import java.util.ArrayList;

public class Buttons {

    private ArrayList<JButton> buttons = new ArrayList<JButton>();
    private ArrayList<JButton> menu_buttons = new ArrayList<JButton>();

    public ArrayList<JButton> getMenuButtons() {
        menu_buttons.add(new JButton("Math"));
        menu_buttons.add(new JButton("Stat"));
        menu_buttons.add(new JButton("PI"));
        menu_buttons.add(new JButton("Fx"));

        return menu_buttons;
    }
    public ArrayList<JButton> getButtonList() {

        buttons.add(new JButton("cos"));
        buttons.add(new JButton("sin"));
        buttons.add(new JButton("tan"));
        buttons.add(new JButton("log"));
        buttons.add(new JButton("x"));
        buttons.add(new JButton("x2"));
        buttons.add(new JButton("("));
        buttons.add(new JButton(")"));
        buttons.add(new JButton("AC"));
        buttons.add(new JButton("/"));
        buttons.add(new JButton("\u00D7"));
        buttons.add(new JButton("\253"));
        buttons.add(new JButton("7"));
        buttons.add(new JButton("8"));
        buttons.add(new JButton("9"));
        buttons.add(new JButton("-"));
        buttons.add(new JButton("4"));
        buttons.add(new JButton("5"));
        buttons.add(new JButton("6"));
        buttons.add(new JButton("+"));
        buttons.add(new JButton("1"));
        buttons.add(new JButton("2"));
        buttons.add(new JButton("3"));
        buttons.add(new JButton("%"));
        buttons.add(new JButton("."));
        buttons.add(new JButton("0"));
        buttons.add(new JButton("(-)"));
        buttons.add(new JButton("="));

        return buttons;
    }
}

