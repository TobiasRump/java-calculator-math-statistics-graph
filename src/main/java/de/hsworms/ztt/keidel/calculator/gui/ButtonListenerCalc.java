package de.hsworms.ztt.keidel.calculator.gui;

import de.hsworms.ztt.keidel.calculator.ReversePolishNotation;
import de.hsworms.ztt.keidel.calculator.graphic.JPanelLineChart;
import de.hsworms.ztt.keidel.calculator.logarithmus.FilterLogarithm;
import de.hsworms.ztt.keidel.calculator.monteCarloSimulation.MonteCarloChart;
import de.hsworms.ztt.keidel.calculator.statistics.StatisticFunctions;
import de.hsworms.ztt.keidel.calculator.statistics.StatisticTableGUI;
import de.hsworms.ztt.keidel.calculator.trigonometricFunction.FilterTrigonometricFunction;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ButtonListenerCalc implements ActionListener {

    private JTextField label_from_window;
    private JPanel display = null;
    private Mode current_mode = Mode.MATH;
    private JPanelLineChart lineChart = new JPanelLineChart();
    private StatisticTableGUI stat = new StatisticTableGUI();
    private JScrollPane math_window;

    public ButtonListenerCalc(JTextField label_input, JScrollPane input_scroll) throws IOException, FontFormatException {
        label_from_window = label_input;
        math_window = input_scroll;
    }

    //different calculator function modis
    private enum Mode {MATH, STAT, PI, FX}

    //button click functions

    /**
     * handling of all button functionalities on the calculator
     * and mode change functionalities
     * @param e Buttonlistener-Event
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        String butto_name = e.getActionCommand();

        switch (butto_name) {
            case "0":
            case "1":
            case "2":
            case "3":
            case "4":
            case "5":
            case "6":
            case "7":
            case "8":
            case "9":
            case "(":
            case ")":
            case "x":
            case ".":
                setLabelforExpression(butto_name);
                break;
            case "x2":
                setLabelforExpression("(x * x)");
                break;
            case "(-)":
                setLabelforExpression("-");
                break;
            case "-":
                setLabelforExpression(" - ");
                break;
            case "+":
                setLabelforExpression(" + ");
                break;
            case "%":
                setLabelforExpression(" % ");
                break;
            case "/":
                setLabelforExpression(" / ");
                break;
            case "\u00D7":
                setLabelforExpression(" * ");
                break;
            case "cos":
                setLabelforExpression("cos(");
                break;
            case "tan":
                setLabelforExpression("tan(");
                break;
            case "sin":
                setLabelforExpression("sin(");
                break;
            case "log":
                setLabelforExpression("log(");
                break;
            case "AC":
                clearLabelonScreen();
                break;
            case "\253":
                delOneCharOnLabelonScreen();
                break;
            case "=":
                printResultLabelonScreen();
                break;
            case "PI":
                current_mode = Mode.PI;
                openMonteCarloSimulation();
                break;
            case "Stat":
                openStatisticScreen();
                current_mode = Mode.STAT;
                break;
            case "Math":
                current_mode = Mode.MATH;
                openMathCalculation();
                break;
            case "Fx":
                current_mode = Mode.FX;
                openPowerFunctionCalculation("");
                break;
            default:
                System.exit(0);
                break;
        }
    }
    
    private void openMathCalculation() {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                display.removeAll();
                display.add(math_window);
                display.updateUI();
            }
        });
    }

    private void openStatisticScreen() {

        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                display.removeAll();
                display.add(stat);
                display.updateUI();
            }
        });
    }

    private void openPowerFunctionCalculation(String infix) {
        if (display == null) return;


        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                display.removeAll();
                try {
                    display.add(lineChart.createLineChart(infix), BorderLayout.CENTER);
                } catch (IOException | FontFormatException e) {
                    e.printStackTrace();
                }
                display.updateUI();
            }
        });
    }

    private void openMonteCarloSimulation() {
        MonteCarloChart chart = new MonteCarloChart();

        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                display.removeAll();
                display.add(chart.getMonteCarloPanel());
                display.updateUI();
            }
        });
    }

    /**
     * Display for entering the mathematical expression
     * @param new_label_char represent the sign of a pressed Button
     */
    public void setLabelforExpression(String new_label_char) {
        String math_expression;

        //The math expression need to be handled different in each calculator mode
        switch (current_mode) {
            case MATH:
                math_expression = label_from_window.getText() + new_label_char;
                label_from_window.setText(math_expression);
                break;

            case STAT:
                JTable table = stat.getStatisticTable();

                //get position of the selected cell
                if (table.getSelectedColumn() != -1 && table.getSelectedRow() != -1) {
                    int row_index = table.getSelectedRow();
                    int col_index = table.getSelectedColumn();

                    //get the content of the cell & add one new number
                    String newContent = table.getModel().getValueAt(row_index, col_index) + new_label_char;

                    table.setValueAt(newContent, row_index, col_index);
                }
                break;

            case FX:
                math_expression = lineChart.getOutputLabel().getText() + new_label_char;
                lineChart.getOutputLabel().setText(math_expression);
                break;
        }

    }

    public void clearLabelonScreen() {
        switch (current_mode) {
            case MATH:
                label_from_window.setText("");
                break;

            case FX:
                openPowerFunctionCalculation("");
                break;
        }
    }

    public void delOneCharOnLabelonScreen() {
        String current_expression;

        switch (current_mode) {
            case MATH:
                current_expression = label_from_window.getText();

                if (current_expression.length() > 0){
                    label_from_window.setText(current_expression.substring(0, current_expression.length() - 1));
                }
                break;

            case FX:
                current_expression = lineChart.getOutputLabel().getText();
                if (current_expression.length() > 0) {
                    lineChart.getOutputLabel().setText(current_expression.substring(0, current_expression.length() - 1));
                }
                break;
        }
    }

    public void printResultLabelonScreen() {

        switch (current_mode) {
            case MATH:
                String current_expression = label_from_window.getText();

                double result = 0;

                if(IsNegativeSignWithoutSpaceInExpression(current_expression)){
                    label_from_window.setText("NO INPUT");
                    return;
                }

                if (current_expression.isEmpty() || current_expression.equals("NO INPUT")) {
                    label_from_window.setText("NO INPUT");
                    return;
                }
                try {
                    //checks the infix according to logarithm and calculates it
                    while (FilterLogarithm.IsLogarithm(current_expression))
                        current_expression = FilterLogarithm.GetReplacedInfixString(current_expression);

                    //checks the infix according to sin/cos/tan and calculates it
                    while(FilterTrigonometricFunction.IsTrigonometricFunctionAvailable(current_expression))
                        current_expression = FilterTrigonometricFunction.GetReplacedInfixString(current_expression);

                    result = ReversePolishNotation.getResult(current_expression);
                } catch (IOException e) {
                    label_from_window.setText("ERROR");
                }
                catch (IllegalStateException e)
                {
                    label_from_window.setText("ERROR");
                }
                label_from_window.setText(String.valueOf(result));
                break;

            case STAT:
                JTable table = stat.getStatisticTable();

                int row_index = table.getRowCount();

                //String for the output
                Object[] result_list = new Object[row_index];

                //EditFocus will be set to position (0,0), to save the edits
                table.editCellAt(0, 0);

                //delete the focus
                table.removeEditor();

                for (int i = 0; i < row_index; i++) {
                    if (table.getModel().getValueAt(i, 1) == "") result_list[i] = 0;
                    else result_list[i] = table.getModel().getValueAt(i, 1);
                }

                StatisticFunctions solve_statistic = new StatisticFunctions(result_list);

                SwingUtilities.invokeLater(new Runnable() {
                    public void run() {
                        display.removeAll();
                        display.add(solve_statistic.printedStatisticResults());
                        display.updateUI();
                    }
                });
                break;

            case FX:
                openPowerFunctionCalculation(lineChart.getOutputLabel().getText());
                break;
        }
    }

    private boolean IsNegativeSignWithoutSpaceInExpression(String expression) {

        String pattern = "[^ ]-";
        Pattern r = Pattern.compile(pattern);
        Matcher m = r.matcher(expression);

        if (m.find( ))
            return true;
        else
            return false;
    }

    public void setDisplayJPanel(JPanel display_in) {
        display = display_in;
    }
}
