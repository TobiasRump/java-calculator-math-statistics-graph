package de.hsworms.ztt.keidel.calculator.statistics;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.security.InvalidParameterException;
import java.util.Locale;

public class StatisticFunctions {

    private Object[] new_data;

    public StatisticFunctions(Object[] input_Data) {
        this.new_data = input_Data;
    }


    public double getArithmeticMean() {
        double mean = 0;
        int divider = new_data.length;

        for (Object new_data : new_data) {
            if (new_data instanceof String) {
                mean += Double.parseDouble((String) new_data);
            } else {
                mean += (int) new_data;
            }
        }

        if (mean == 0f || new_data.length == 0) return 0f;

        return mean / divider;
    }

    /**
     *
     * @return the total of the values from the table
     */
    public double getTotalX() {
        double total = 0;

        for (Object new_data : new_data) {
            if (new_data instanceof String) {
                total += Double.parseDouble((String) new_data);
            } else {
                total += (int) new_data;    //handle for empty table
            }

        }
        return total;
    }

    public double getTotalXSquared() {
        double sum = 0;

        for (Object new_data : new_data) {
            if (new_data instanceof String) {
                sum += Math.pow(Double.parseDouble((String) new_data), 2);
            } else {
                sum += (int) new_data;  //handle for empty table
            }
        }
        return sum;
    }

    /**
     *
     * @return the minimum Value from the table
     */
    public double getMinValue() {
        double min = Double.parseDouble(new_data[0].toString());

        for (int i = 1; i < new_data.length; i++) {

            if (new_data[i] instanceof String) {
                if (Double.parseDouble(new_data[i].toString()) < min)
                    min = Double.parseDouble(new_data[i].toString());
            } else {
                min += (int) new_data[i];   //handle for empty table
            }
        }
        return min;
    }

    /**
     *
     * @return the maximum Value from the table
     */
    public double getMaxValue() {
        double max = Double.parseDouble(new_data[0].toString());

        for (int i = 1; i < new_data.length; i++) {
            if (new_data[i] instanceof String) {
                if (Double.parseDouble(new_data[i].toString()) > max)
                    max = Double.parseDouble(new_data[i].toString());
            } else {
                max += (int) new_data[i];   //handle for empty table
            }
        }
        return max;
    }

    public double getEmpiricalVariance() {
        double empirical_variance = 0;
        double mean = getArithmeticMean();

        if (mean != 0f) {
            for (int i = 1; i < new_data.length; i++) {
                if (new_data[i] instanceof String) {
                    empirical_variance += Math.pow(Double.parseDouble((String) new_data[i]) - mean, 2);
                }
            }
            return Double.parseDouble(String.format(Locale.ENGLISH, "%1.4f", (1.0 / new_data.length) * empirical_variance));
        } else return 0f;
    }

    public double getStandardDeviation() {
        return Double.parseDouble(String.format(Locale.ENGLISH, "%1.4f", Math.sqrt(getEmpiricalVariance())));
    }

    public int getCountOfElements() {
        int count = 0;
        for (Object new_data : new_data) {
            if (new_data instanceof String) {
                count++;
            }
        }
        return count;
    }

    public double getLowerQuartile() {
        return CalculateQuartile(0.25f);

    }

    public double getUpperQuartile() {
        return CalculateQuartile(0.75f);

    }

    public double getMedian() {
        return CalculateQuartile(0.5f);
    }

    /**
     * Bubble-Sort
     * is needed to sort the list for the Quartile
     * @param filtered_data
     */
    private void helperSort(double[] filtered_data) {
        double cache_double;
        for (int index = 1; index < filtered_data.length; index++)
        {
            for (int sub_index = 0; sub_index < filtered_data.length - index; sub_index++)
            {
                if (filtered_data[sub_index] > filtered_data[sub_index + 1])
                {
                    cache_double = filtered_data[sub_index];
                    filtered_data[sub_index] = filtered_data[sub_index + 1];
                    filtered_data[sub_index + 1] = cache_double;
                }
            }
        }
    }

    private double CalculateQuartile(float quartil_index) {

        final int ELEMENTS = new_data.length;
        double[] filtered_data = new double[ELEMENTS];
        int quartile_pos;

        for (int index = 0; index < ELEMENTS; index++) {
            filtered_data[index] = Double.parseDouble(new_data[index].toString());
        }

        if (filtered_data.length < 2) return filtered_data[ELEMENTS - 1];

        helperSort(filtered_data);

        //Decide which quartile should be calculate in call
        if (quartil_index == 0.5f) {
            if (ELEMENTS % 2 == 0) {
                double first_element = filtered_data[(ELEMENTS / 2) - 1];
                double second_element = filtered_data[(ELEMENTS / 2)];
                return (int) (0.5 * (first_element + second_element));
            } else {
                quartile_pos = (ELEMENTS + 1) / 2;
                return filtered_data[quartile_pos];
            }
        } else if (quartil_index == 0.25f || quartil_index == 0.75f) {
            quartile_pos = Math.round(quartil_index * ELEMENTS) - 1;
            return filtered_data[quartile_pos];
        } else {
            throw new InvalidParameterException("Error [" + quartil_index + "] quartile parameter not allows");
        }
    }

    public JPanel printedStatisticResults() {
        String results = "x\u0305\t" + getArithmeticMean() + "\n" +
                "\u2211x\t" + getTotalX() + "\n" +
                "\u2211x\u00B2\t" + getTotalXSquared() + "\n" +
                "o\u00B2x\t" + getEmpiricalVariance() + "\n" +
                "ox\t" + getStandardDeviation() + "\n" +
                "n\t" + getCountOfElements() + "\n" +
                "min(x)\t" + getMinValue() + "\n" +
                "Q(0.25)\t" + getLowerQuartile() + "\n" +
                "Med\t" + getMedian() + "\n" +
                "Q(0.75)\t" + getUpperQuartile() + "\n" +
                "max(x)\t" + getMaxValue() + "\n";

        //ScreenArea for Output Results
        JTextArea result_print = new JTextArea(results);
        result_print.setFont(new Font("Arial", Font.PLAIN, 14));
        result_print.setBackground(new Color(162, 175, 119));

        JPanel result_panel = new JPanel();
        result_panel.setPreferredSize(new Dimension(345, 110));
        result_panel.setBackground(new Color(162, 175, 119));

        JScrollPane scroll_view_for_results = new JScrollPane(result_print);
        Border border = BorderFactory.createEmptyBorder(0, 0, 0, 0);
        scroll_view_for_results.setBorder(border);
        scroll_view_for_results.setPreferredSize(new Dimension(220, 110));
        scroll_view_for_results.setAutoscrolls(true);

        result_panel.add(scroll_view_for_results);
        return result_panel;
    }
}
