package de.hsworms.ztt.keidel.calculator.gui;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.RoundRectangle2D;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;


public class CalculatorWindow extends JFrame {

    private Point last_mouse_position = new Point();
    private final Font custom_font;
    private final Buttons gui_elements = new Buttons();
    private final ArrayList<JButton> menu_buttons_calculator = gui_elements.getMenuButtons();
    private final JTextField math_expression_text_area = new JTextField();
    private final JScrollPane scroll_for_expression = new JScrollPane(math_expression_text_area);
    private final ButtonListenerCalc button_listener_calc = new ButtonListenerCalc(math_expression_text_area, scroll_for_expression);


    public CalculatorWindow() throws IOException, FontFormatException {

        //Window Elements
        String font_path = System.getProperty("user.dir") + "\\src\\main\\java\\de\\hsworms\\ztt\\keidel\\calculator\\gui\\DS-DIGI.TTF";
        String exit_button_path = System.getProperty("user.dir") + "\\src\\main\\java\\de\\hsworms\\ztt\\keidel\\calculator\\gui\\exit_button.png";
        JPanel header_panel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        JPanel display_panel = new JPanel();
        ArrayList<JButton> buttons_calculator = gui_elements.getButtonList();
        JPanel padding_between_number_n_display = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        JButton exit_button = new JButton(new ImageIcon(exit_button_path));
        JPanel calculator_panel = new JPanel();

        //Implemented and Register a new Font for a Digital Design onScreen
        custom_font = Font.createFont(Font.TRUETYPE_FONT, new File(font_path)).deriveFont(62f);
        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        ge.registerFont(custom_font);

        //Configs
        setWindowConfigurations();
        setCalculatorPanelConfigurations(calculator_panel);
        setHeaderPanelConfigurations(header_panel);
        setExitButtonConfigurations(exit_button);
        setDisplayPanelConfigurations(display_panel);
        setPaddingPanelConfigurations(padding_between_number_n_display);
        setMathExpressionTextAreaConfigurations(math_expression_text_area);
        setScrollBarForPanelConfiguratioens(scroll_for_expression);

        //Designbuild Hierarchy
        add(calculator_panel);
            calculator_panel.add(header_panel);
                header_panel.add(exit_button);
                    exit_button.addActionListener(button_listener_calc);
                        button_listener_calc.setDisplayJPanel(display_panel);
            calculator_panel.add(display_panel);
                    display_panel.add(scroll_for_expression, BorderLayout.NORTH);
            calculator_panel.add(padding_between_number_n_display);
            for (JButton button : buttons_calculator) {
                 setButtonStyle(button);
                 button.addActionListener(button_listener_calc);
                calculator_panel.add(button);
            }
    }

    public void runCalculator() {
        this.setVisible(true);
    }

    private void setWindowConfigurations() {
        setTitle("Taschenrechner FPK-SoSe2020");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //close window
        setUndecorated(true);
        Dimension min_display_size = new Dimension(400, 625);
        setSize(min_display_size);
        setResizable(false);
        setLocationRelativeTo(null);

        //Set option to drag and drop the calculator window
        addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                setShape(new RoundRectangle2D.Double(0, 0, getWidth(), getHeight(), 70, 70));
            }
        });
        addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent event) {
                if (event.getButton() == MouseEvent.BUTTON1) //mouse button left
                {
                    last_mouse_position = new Point(event.getX(), event.getY());
                }
            }
        });
        addMouseMotionListener(new MouseMotionAdapter() {
            public void mouseDragged(MouseEvent e) {
                if (MouseInfo.getPointerInfo() != null) {
                    PointerInfo pointer_info = MouseInfo.getPointerInfo();
                    Point b = pointer_info.getLocation();
                    int x = (int) (b.getX() - last_mouse_position.getX());
                    int y = (int) (b.getY() - last_mouse_position.getY());
                    setLocation(x, y);
                }
            }
        });
    }

    private void setCalculatorPanelConfigurations(JPanel calculator_panel) {
        calculator_panel.setBackground(new Color(57, 57, 59));
    }

    private void setHeaderPanelConfigurations(JPanel header) {
        header.setPreferredSize(new Dimension(350, 35));
        header.setBackground(new Color(57, 57, 59));
    }

    private void setExitButtonConfigurations(JButton exit_button) {
        exit_button.setHorizontalTextPosition(JButton.CENTER);
        exit_button.setVerticalTextPosition(JButton.CENTER);
        exit_button.setOpaque(false);
        exit_button.setContentAreaFilled(false);
        exit_button.setBorderPainted(false);
        Border empty_border = BorderFactory.createEmptyBorder();
        exit_button.setBorder(empty_border);
    }

    private void setButtonStyle(JButton button_to_style) {
        Border line = new LineBorder(Color.BLACK);
        Border margin = new EmptyBorder(5, 15, 5, 15);
        Border compound = new CompoundBorder(line, margin);

        button_to_style.setFont(new Font("Arial", Font.PLAIN, 30));
        button_to_style.setForeground(Color.WHITE);
        button_to_style.setBackground(new Color(88, 89, 91));
        button_to_style.setPreferredSize(new Dimension(80, 50));
        button_to_style.setBorder(compound);

        //All Buttons have the same design except zero-button
        switch (button_to_style.getText()) {
            case "=": {
                button_to_style.setBackground(new Color(241, 90, 43));
                break;
            }
        }
    }

    private void setMenuButtonStyle(JButton button_to_style) {
        Border line = new LineBorder(Color.BLACK);
        Border margin = new EmptyBorder(5, 15, 5, 15);
        Border compound = new CompoundBorder(line, margin);

        button_to_style.setFont(new Font("Arial", Font.PLAIN, 10));
        button_to_style.setForeground(Color.WHITE);
        button_to_style.setBackground(Color.DARK_GRAY);
        button_to_style.setPreferredSize(new Dimension(80, 20));
        button_to_style.setBorder(compound);
    }

    private void setDisplayPanelConfigurations(JPanel display) {
        display.setOpaque(false);
        display.setBackground(new Color(162, 175, 119));
    }

    private void setPaddingPanelConfigurations(JPanel padding_between_number_n_display) {
        for (JButton button : menu_buttons_calculator) {
            setMenuButtonStyle(button);
            button.addActionListener(button_listener_calc);
            padding_between_number_n_display.add(button);
        }
        padding_between_number_n_display.setPreferredSize(new Dimension(345, 30));
        padding_between_number_n_display.setBackground(new Color(57, 57, 59));
    }

    private void setMathExpressionTextAreaConfigurations(JTextField text_area) {
        text_area.setFont(custom_font);
        text_area.setAutoscrolls(true);
        text_area.setHorizontalAlignment(JTextField.RIGHT);
        text_area.setBackground(new Color(162, 175, 119));
        text_area.setEditable(false);
    }

    private void setScrollBarForPanelConfiguratioens(JScrollPane scroll) {
        scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
        scroll.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        scroll.setPreferredSize(new Dimension(335, 120));
    }
}
