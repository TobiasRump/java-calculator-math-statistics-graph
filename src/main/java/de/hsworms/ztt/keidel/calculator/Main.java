package de.hsworms.ztt.keidel.calculator;

import de.hsworms.ztt.keidel.calculator.gui.CalculatorWindow;

import java.awt.*;
import java.io.IOException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws IOException, FontFormatException
    {
        CalculatorWindow window = new CalculatorWindow();
        window.runCalculator();
    }
}
