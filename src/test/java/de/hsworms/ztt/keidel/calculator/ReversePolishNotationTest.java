package de.hsworms.ztt.keidel.calculator;

import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.*;

public class ReversePolishNotationTest {

    private static final double DELTA = 0.0000001;

    @Test
    public void getResult() throws IOException {
        assertEquals(2.0, ReversePolishNotation.getResult("(1+1)"), DELTA);
        assertEquals(4.0, ReversePolishNotation.getResult("(1+1)*2"), DELTA);
        assertEquals(2.0, ReversePolishNotation.getResult("(1+1)*(2 - 1)"), DELTA);
        assertEquals(121.0, ReversePolishNotation.getResult("(120+1)*(2 - 1)"), DELTA);
        assertEquals(121.0, ReversePolishNotation.getResult("(120+1)/(2 - 1)"), DELTA);
        assertEquals(12.0, ReversePolishNotation.getResult("((120+1) - 1)/(11 - 1)"), DELTA);
        assertEquals(0., ReversePolishNotation.getResult("(   1 - 1)"), DELTA);
        assertEquals(3.0, ReversePolishNotation.getResult("(   1 - -2)"), DELTA);
        assertEquals(-1.0, ReversePolishNotation.getResult("(   1.1 + -2.1)"), DELTA);
        assertEquals(0.5, ReversePolishNotation.getResult("(   1.0 / 2.0)"), DELTA);
        assertEquals(3.0, ReversePolishNotation.getResult("1 + 1 * 2"), DELTA);
        assertEquals(2.0, ReversePolishNotation.getResult("1 + 1 * 2 / 2"), DELTA);
        assertEquals(3.0, ReversePolishNotation.getResult("1 + 1 * 2 / 2 + 1"), DELTA);

        assertEquals(4.0, ReversePolishNotation.getResult("2 * 2"), DELTA);
        assertEquals(16.0, ReversePolishNotation.getResult("4 * 4"), DELTA);
        assertEquals(25.0, ReversePolishNotation.getResult("5 * 5"), DELTA);

        // Praktikum task: Modulo
        assertEquals(0., ReversePolishNotation.getResult("2 % 2"), DELTA);
        assertEquals(1., ReversePolishNotation.getResult("1 % 2"), DELTA);
        assertEquals(0., ReversePolishNotation.getResult("0 % 2"), DELTA);
        assertEquals(2., ReversePolishNotation.getResult("1 + 1 % 2"), DELTA);
        assertEquals(0., ReversePolishNotation.getResult("(1 + 1) % 2"), DELTA);
        assertEquals(0., ReversePolishNotation.getResult("(1 + 1) % (1 + 1)"), DELTA);
        assertEquals(0., ReversePolishNotation.getResult("(1023 + 1) % (1 + 1)"), DELTA);
    }
}