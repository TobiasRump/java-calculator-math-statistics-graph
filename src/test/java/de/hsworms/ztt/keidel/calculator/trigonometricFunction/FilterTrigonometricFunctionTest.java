package de.hsworms.ztt.keidel.calculator.trigonometricFunction;

import org.junit.Test;

import static org.junit.Assert.*;

public class FilterTrigonometricFunctionTest {

    @Test
    public void isTrigonometricFunctionAvailable() {
        assertTrue(FilterTrigonometricFunction.IsTrigonometricFunctionAvailable("3 * sin(2 + 2) / 2"));
        assertTrue(FilterTrigonometricFunction.IsTrigonometricFunctionAvailable("3 * cos(2 + 2) / 2"));
        assertTrue(FilterTrigonometricFunction.IsTrigonometricFunctionAvailable("3 * tan(2 + 2) / 2"));
    }

    @Test
    public void getReplacedInfixString() {
        assertEquals("TEST PASSED", "3 * 1.0", FilterTrigonometricFunction.GetReplacedInfixString("3 * sin(45+45)"));

        String temp = "3 * sin(45 + 45) * cos(45 + 45) * tan(45 + 44)";
        while(FilterTrigonometricFunction.IsTrigonometricFunctionAvailable(temp))
        {
            temp = FilterTrigonometricFunction.GetReplacedInfixString(temp);
        }
        assertEquals("TEST PASSED", "3 * 1.0 * 0.0 * 57.29", temp);
    }

    @Test
    public void round() {
        assertEquals("PI-TEST PASSED", 3.14, FilterTrigonometricFunction.round(Math.PI, 2));
    }
}