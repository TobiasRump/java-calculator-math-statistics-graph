package de.hsworms.ztt.keidel.calculator.statistics;

import org.junit.Test;

import static org.junit.Assert.*;

public class StatisticFunctionsTest {

    private Object[] soft_test_data = {"10","20","15","14","2","1"};

    private Object[] hard_test_data = {"9123124", "2123731","1123132","1432295","2234273","23423282",};

    private static final double FOUR_DESCENDANTS = 0.0001;
    private StatisticFunctions function_collection_for_soft_test = new StatisticFunctions(soft_test_data);
    private StatisticFunctions function_collection_for_hard_test = new StatisticFunctions(hard_test_data);

    @Test
    public void getArithmeticMean() {
        assertEquals(10.3333, function_collection_for_soft_test.getArithmeticMean(), FOUR_DESCENDANTS);
        assertEquals(6_576_639.5, function_collection_for_hard_test.getArithmeticMean(), FOUR_DESCENDANTS);
    }

    @Test
    public void getTotalX() {
        assertEquals(62, function_collection_for_soft_test.getTotalX(), FOUR_DESCENDANTS);
        assertEquals(39_459_837, function_collection_for_hard_test.getTotalX(), FOUR_DESCENDANTS);
    }

    @Test
    public void getTotalXSquared() {
        assertEquals(926, function_collection_for_soft_test.getTotalXSquared(), FOUR_DESCENDANTS);
        assertEquals(6_446_966_348_262_39.0, function_collection_for_hard_test.getTotalXSquared(), FOUR_DESCENDANTS);
    }

    @Test
    public void getMinValue() {
        assertEquals(1, function_collection_for_soft_test.getMinValue(), FOUR_DESCENDANTS);
        assertEquals(1123132.0, function_collection_for_hard_test.getMinValue(), FOUR_DESCENDANTS);
    }

    @Test
    public void getMaxValue() {
        assertEquals(20, function_collection_for_soft_test.getMaxValue(), FOUR_DESCENDANTS);
        assertEquals(23423282, function_collection_for_hard_test.getMaxValue(), FOUR_DESCENDANTS);
    }

    @Test
    public void getEmpiricalVariance() {
        assertEquals(47.537, function_collection_for_soft_test.getEmpiricalVariance(), FOUR_DESCENDANTS);
        assertEquals(63_116_488_139_956.2, function_collection_for_hard_test.getEmpiricalVariance(), FOUR_DESCENDANTS);
    }

    @Test
    public void getStandardDeviation() {
        assertEquals(6.8947, function_collection_for_soft_test.getStandardDeviation(), FOUR_DESCENDANTS);
        assertEquals(7_944_588.6073, function_collection_for_hard_test.getStandardDeviation(), FOUR_DESCENDANTS);
    }

    @Test
    public void getCountOfElements() {
        assertEquals(6, function_collection_for_soft_test.getCountOfElements(), FOUR_DESCENDANTS);
        assertEquals(6, function_collection_for_hard_test.getCountOfElements(), FOUR_DESCENDANTS);
    }

    @Test
    public void getLowerQuartile() {
        assertEquals(2, function_collection_for_soft_test.getLowerQuartile(), FOUR_DESCENDANTS);
        assertEquals(1432295.0, function_collection_for_hard_test.getLowerQuartile(), FOUR_DESCENDANTS);
    }

    @Test
    public void getUpperQuargtile() {
        assertEquals(15, function_collection_for_soft_test.getUpperQuartile(), FOUR_DESCENDANTS);
        assertEquals(9123124.0, function_collection_for_hard_test.getUpperQuartile(), FOUR_DESCENDANTS);
    }

    @Test
    public void getMedian() {
        assertEquals(12, function_collection_for_soft_test.getMedian(), FOUR_DESCENDANTS);
        assertEquals(2179002.0, function_collection_for_hard_test.getMedian(), FOUR_DESCENDANTS);
    }


}