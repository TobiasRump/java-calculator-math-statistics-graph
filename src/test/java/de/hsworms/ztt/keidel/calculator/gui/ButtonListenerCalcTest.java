package de.hsworms.ztt.keidel.calculator.gui;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.swing.*;

import static org.junit.Assert.*;

public class ButtonListenerCalcTest {

    private JTextField label_from_window;
    private ButtonListenerCalc buttonListenerCalc;
    private JScrollPane scrollPane;

    @Before
    public void setUp() throws Exception {
        label_from_window = new JTextField();
        buttonListenerCalc = new ButtonListenerCalc(label_from_window,scrollPane);
    }

    @After
    public void tearDown() throws Exception {
        label_from_window = null;
        buttonListenerCalc = null;
    }

    @Test
    public void setLabelforExpression() {
        //Arrange
        String new_number_1;

        //Act
        new_number_1 = "1";
        label_from_window.setText("");
        buttonListenerCalc.setLabelforExpression(new_number_1);

        //Assert
        assertEquals(label_from_window.getText(), "1");
    }

    @Test
    public void clearLabelonScreen() {
        //Arrange
        String empty_string;

        //Act
        empty_string = "";
        label_from_window.setText("12+12");
        buttonListenerCalc.clearLabelonScreen();

        //Assert
        assertEquals(label_from_window.getText(), empty_string);
    }

    @Test
    public void delOneCharOnLabelonScreen() {
        //Arrange


        //Act
        label_from_window.setText("15+15");
        buttonListenerCalc.delOneCharOnLabelonScreen();

        //Assert
        assertEquals(label_from_window.getText(), "15+1");
    }

    @Test
    public void printResultLabelonScreen(){
        //Arrange
        Float result;

        //Act
        result = 30f;
        label_from_window.setText("2*(3+12)");
        buttonListenerCalc.printResultLabelonScreen();


        //Assert
        assertEquals(label_from_window.getText(), result.toString());
    }
}
