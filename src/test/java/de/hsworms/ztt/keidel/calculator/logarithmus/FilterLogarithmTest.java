package de.hsworms.ztt.keidel.calculator.logarithmus;

import de.hsworms.ztt.keidel.calculator.trigonometricFunction.FilterTrigonometricFunction;
import org.junit.Test;

import static org.junit.Assert.*;

public class FilterLogarithmTest {

    @Test
    public void isLogarithm() {
        assertTrue(FilterLogarithm.IsLogarithm("3 * log(2 + 2) / 2"));
    }

    @Test
    public void getReplacedInfixString() {
        assertEquals("TEST PASSED", "3 * 4.5", FilterLogarithm.GetReplacedInfixString("3 * log(45+45)"));

        String temp = "3 * log(45 + 45) * log(45 + 100) * log(45 - 44)";
        while(FilterLogarithm.IsLogarithm(temp))
        {
            temp = FilterLogarithm.GetReplacedInfixString(temp);
        }
        assertEquals("TEST PASSED", "3 * 4.5 * 4.98 * 0.0", temp);
    }

    @Test
    public void round() {
        assertEquals(3.14, FilterLogarithm.round(Math.PI, 2), 0);

    }
}